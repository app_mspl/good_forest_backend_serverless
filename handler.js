
const aws = require('aws-sdk')

const myEmail = process.env.EMAIL
const myDomain = process.env.DOMAIN

const SESCofig= {
  apiVersion:'2010-12-01',
  region:process.env.AWS_SES_REGION,
  accessKeyId:process.env.AWS_SES_ACCESS_KEY,
  secretAccessKey:process.env.AWS_SES_SECRET_KEY,
}

const ses = new aws.SES(SESCofig);

function generateResponse (code, payload) {
  return {
    statusCode: code,
    headers: {
      'Access-Control-Allow-Origin': "*",
      'Access-Control-Allow-Headers': 'x-requested-with',
      'Access-Control-Allow-Credentials': true
    },
    body: JSON.stringify({ status: true })
  }
}

function generateError (code, err) {
  console.log(err)
  return {
    statusCode: code,
    headers: {
      'Access-Control-Allow-Origin': "*",
      'Access-Control-Allow-Headers': 'x-requested-with',
      'Access-Control-Allow-Credentials': true
    },
    //body: JSON.stringify(err.message)
    body: JSON.stringify({ status: false,'message':err })
  }
}

function generateEmailParams (body) {
  const { subject,email, content } = JSON.parse(body)

  if (!(email && content && subject)) {
    throw new Error('Missing parameters! Make sure to add parameters \'email\', \'name\', \'content\'.')
  }

  return {
    Source: myEmail,
    Destination: { ToAddresses: [email] },
    ReplyToAddresses: [myEmail],
    Message: {
      /*Body: {
        Html: {
          Charset: "UTF-8",
          Data: htmlBody
        },
        Text: {
          Charset: "UTF-8",
          Data: textBody
        }
      },*/
      Body: {
        Html: {
          Charset: 'UTF-8',
          //Data: `Message sent from email ${email} by ${name} \nContent: ${content}`
          Data: content
        }
      },
      Subject: {
        Charset: 'UTF-8',
        Data: subject
      }
    }
  }
}

module.exports.send = async (event) => {
  try {
    const emailParams = generateEmailParams(event.body)
    const data = await ses.sendEmail(emailParams).promise()
    return generateResponse(200, data)
  } catch (err) {
    return generateError(500, err)
  }
}