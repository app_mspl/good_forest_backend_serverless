'use strict';

const dynamodb = require('../dynamodb');

module.exports.list = (event, context, callback) => {

  const params = {
    TableName:process.env.DYNAMODB_APPLICATION_TABLE,
    IndexName: "status-user_id-index",
    KeyConditionExpression:'#status = :value',
    ExpressionAttributeNames: {
      '#status': 'status',
    },
    ExpressionAttributeValues:{
      ':value': event.pathParameters.status,
    },
    ScanIndexForward:false,
  };

  // Set response headers to enable CORS (Cross-Origin Resource Sharing)
  const headers = {
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Allow-Credentials": true
  };
  
  //dynamodb.scan(params, (error, result) => {

  dynamodb.query(params, (error, result) => {
    // handle potential errors
    if (error) {
      console.error(error);
      callback(null, {
        statusCode: error.statusCode || 501,
        headers: headers,
        body: JSON.stringify({ status: false ,'message':error})
      });
      return;
    }

    // create a response
    const response = {
      statusCode: 200,
      headers: headers,
      body: JSON.stringify(result),
    };
    callback(null, response);
  });
};
