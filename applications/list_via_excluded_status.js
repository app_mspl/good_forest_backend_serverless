'use strict';

const dynamodb = require('../dynamodb');

module.exports.excluded_list = (event, context, callback) => {
  

  const params = {
    TableName:process.env.DYNAMODB_APPLICATION_TABLE,
    FilterExpression: 'NOT contains(#status, :status)',
    ExpressionAttributeNames: {
      '#status': 'status',
    },
    ExpressionAttributeValues: {
      ":status": event.pathParameters.status
    }
  };

  // Set response headers to enable CORS (Cross-Origin Resource Sharing)
  const headers = {
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Allow-Credentials": true
  };
  
  dynamodb.scan(params, (error, result) => {
    // handle potential errors
    if (error) {
      console.error(error);
      callback(null, {
        statusCode: error.statusCode || 501,
        headers: headers,
        body: JSON.stringify({ status: false ,'message':error})
      });
      return;
    }

    // create a response
    const response = {
      statusCode: 200,
      headers: headers,
      body: JSON.stringify(result),
    };
    callback(null, response);
  });
};
