'use strict';

const dynamodb = require('../dynamodb');

module.exports.status_change = (event, context, callback) => {

  const timestamp = new Date().getTime();
  var data = JSON.parse(event.body);
  
  // Set response headers to enable CORS (Cross-Origin Resource Sharing)
  const headers = {
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Allow-Credentials": true
  };


  const update_application_params = {
    TableName: process.env.DYNAMODB_APPLICATION_TABLE,
    Key: {
      user_id: data.user_id,
    },
    ExpressionAttributeNames: {
      '#status': 'status',
    },
    ExpressionAttributeValues: {
      ':updatedAt': timestamp,
      ':status': data.status,        
    },
    UpdateExpression: 'SET #status = :status, updatedAt = :updatedAt',
    ReturnValues: 'ALL_NEW',
  };

 
  dynamodb.update(update_application_params, (error, result) => {
    if (error) {
      console.error(error);
      callback(null, {
        statusCode: error.statusCode || 501,
        headers: headers,
        body: JSON.stringify({ status: false,'message':error })
      });
      return;
    }

    // create a response
    const response = {
      statusCode: 200,
      headers: headers,
      body: JSON.stringify({ status: true ,data:result.Attributes}),
    };
    callback(null, response);
  });
};
