'use strict';

const dynamodb = require('../dynamodb');

module.exports.customer_applications = (event, context, callback) => {

  const params = {
    TableName:process.env.DYNAMODB_APPLICATION_TABLE,
      // Key: {
      //   "user_id = :user_id"
      // },

    // KeyConditionExpression: "user_id = :user_id",
    // ExpressionAttributeValues: {
    //   ":user_id": event.pathParameters.user_id
    // }
    KeyConditionExpression: "user_id = :user_id",
    ExpressionAttributeValues: {
      ":user_id": event.pathParameters.user_id
    }
  };

  // Set response headers to enable CORS (Cross-Origin Resource Sharing)
  const headers = {
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Allow-Credentials": true
  };
  
  dynamodb.query(params, (error, result) => {
    // handle potential errors
    if (error) {
      console.error(error);
      callback(null, {
        statusCode: error.statusCode || 501,
        headers: headers,
        body: JSON.stringify({ status: false ,'message':error})
      });
      return;
    }

    // create a response
    const response = {
      statusCode: 200,
      headers: headers,
      body: JSON.stringify(result.Items[0]),
    };
    callback(null, response);
  });
};
