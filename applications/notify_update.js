'use strict';

const dynamodb = require('../dynamodb');

const timestamp = new Date().getTime();
// Set response headers to enable CORS (Cross-Origin Resource Sharing)
const headers = {
  "Access-Control-Allow-Origin": "*",
  "Access-Control-Allow-Credentials": true
};


module.exports.handler = (event, context, callback) => {
  
  var data = JSON.parse(event.body);

  const update_application_params = {
    TableName: process.env.DYNAMODB_APPLICATION_TABLE,
    Key: {
      user_id: data.user_id,
    },
    // ExpressionAttributeNames: {
    //   '#status': 'status',
    // },
    ExpressionAttributeValues: {
      ':updatedAt': timestamp,
      ':notifications_data': data.notifications_data,   
    },
    UpdateExpression: 'SET notifications_data = :notifications_data, updatedAt = :updatedAt',
    ReturnValues: 'ALL_NEW',
  };

 
  dynamodb.update(update_application_params, (error, result) => {
    if (error) {
      console.error(error);
      callback(null, {
        statusCode: error.statusCode || 501,
        headers: headers,
        body: JSON.stringify({ status: false,'message':error })
      });
      return;
    }

    // create a response
    const response = {
      statusCode: 200,
      headers: headers,
      body: JSON.stringify({ status: true ,data:data.notifications_data}),
    };
    callback(null, response);
  });
};
