'use strict';

const uuid = require('uuid');
const dynamodb = require('../dynamodb');


module.exports.login = (event, context, callback) => {

  const data = JSON.parse(event.body);

  const params = {
      TableName: process.env.DYNAMODB_USER_TABLE,
      FilterExpression: '(contains(main_phone, :username) OR contains(email, :username) ) AND  contains(tax_id, :password)',
      //FilterExpression: 'email = :username and tax_id = :password',
      ExpressionAttributeValues: {
        ':username':data.username,
        ':password':data.password,
      }
  };


  dynamodb.scan(params, (error, result) => {
    // Set response headers to enable CORS (Cross-Origin Resource Sharing)
    const headers = {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Credentials": true
    };
    // Return status code 500 on error
    if (error) {
      const response = {
        statusCode: 500,
        headers: headers,
        body: JSON.stringify({ status: false,'message':error })
      };
      callback(null, response);
      return ;
    }

    if(result.Count){
       // Return status code 200 and the newly created item
      const response = {
        statusCode: 200,
        headers: headers,
        body: JSON.stringify({ status: true,user:result.Items[0]})        
      };
      callback(null, response);
    }else{
       // Return status code 200 and the newly created item
      const response = {
        statusCode: 200,
        headers: headers,
        body: JSON.stringify({ status: false,'message':"no_record"})        
      };
      callback(null, response);
    }   
  });

};

