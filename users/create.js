'use strict';

const uuid = require('uuid');
const dynamodb = require('../dynamodb');

const AWS = require('aws-sdk');
// get reference to S3 client 
var s3 = new AWS.S3();

module.exports.create = (event, context, callback) => {
  const timestamp = new Date().getTime();
  var data = JSON.parse(event.body);
  const ip = event.requestContext.identity.sourceIp;

  /*console.log('---------------------------------------------------------------------------');
  console.log(context);
  console.log('---------------------------------------------------------------------------');

  return false;*/
  
  // Set response headers to enable CORS (Cross-Origin Resource Sharing)
  const headers = {
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Allow-Credentials": true
  };

  if (typeof data.name !== 'string') {
    console.error('Validation Failed');
    callback(null, {
      statusCode: 400,
      headers: headers,
      body: JSON.stringify({ status: false })
    });
    return;
  }


  // images uploading process
  for (var key in data) {
    //console.log(key, data[key]);

    if(key == 'exterior' || key == 'interior'  || key == 'license_images'  || key == 'rental_agreement_images'  || key == 'taipower_bills_images' || key == 'auth_completed_images'){
    

      if (typeof data[key] !== 'undefined' && data[key].length > 0) {

        data[key].forEach(function(value, index) {

          //If an Image is already uploaded
          if(value.path.indexOf(process.env.S3_BUCKET_URI) !== -1){
            return ;
          }

          var image_data=value.path.split(";")
          var extension=image_data[0]
          extension=extension.split(":")
          extension=extension[1]

          var base64result = value.path.substr(value.path.indexOf(',') + 1);
          let encodedImage =base64result;
          let decodedImage = Buffer.from(encodedImage, 'base64');

          var params = {
            "Body": decodedImage,
            "Bucket": process.env.S3_BUCKET_NAME,
            "Key": timestamp+'-'+value.name,
            //"ContentType": "image/jpeg",
            "ContentType": extension,
            "ACL": "public-read",
          }; 

          s3.upload(params, function(error, result) {
            
            if(error) {
              console.error(error);
              callback(null, {
                statusCode: 501,
                headers: headers,
                body: JSON.stringify({ status: false })
              });
              return;
            } else {
              let response = {
                  "statusCode": 200,
                  "body": JSON.stringify(result.key),
                  "isBase64Encoded": false
              };
              return response ;
              //data.exterior[index].path=result.Location
            }
          });
          //replace Image Base64 with S3 link
          //data.exterior[index].path=process.env.S3_BUCKET_URI+params.Key
          data[key][index].path=process.env.S3_BUCKET_URI+params.Key
        });
      }
    }


    if(key == 'lights'){
        for (var lightType in data[key]) {
          //lightData[images].forEach(function(value, index) {
          data[key][lightType].images.forEach(function(value, index) {

            //If an Image is already uploaded
            if(value.path.indexOf(process.env.S3_BUCKET_URI) !== -1){
              return ;
            }

            var image_data=value.path.split(";")
            var extension=image_data[0]
            extension=extension.split(":")
            extension=extension[1]

            var base64result = value.path.substr(value.path.indexOf(',') + 1);
            let encodedImage =base64result;
            let decodedImage = Buffer.from(encodedImage, 'base64');

            var params = {
              "Body": decodedImage,
              "Bucket": process.env.S3_BUCKET_NAME,
              "Key": timestamp+'-'+value.name,
              //"ContentType": "image/jpeg",
              "ContentType": extension,
              "ACL": "public-read",
            }; 

            s3.upload(params, function(error, result) {
              
              if(error) {
                console.error(error);
                callback(null, {
                  statusCode: 501,
                  headers: headers,
                  body: JSON.stringify({ status: false })
                });
                return;
              } else {
                let response = {
                    "statusCode": 200,
                    "body": JSON.stringify(result.key),
                    "isBase64Encoded": false
                };

                //callback(null, response);
                return response ;
                //data.exterior[index].path=result.Location
              }
            });
            //replace Image Base64 with S3 link
            data[key][lightType].images[index].path=process.env.S3_BUCKET_URI+params.Key
          })
        }

      //}
    }
  }
  

  function removeEmptyStringElements(obj) {
    for (var prop in obj) {
      if (typeof obj[prop] === 'object') {// dive deeper in
        removeEmptyStringElements(obj[prop]);
      } else if(obj[prop] === '') {// delete elements that are empty strings
        //delete obj[prop];
        //obj[prop]='na';
        obj[prop]=" ";
      }
    }
    return obj;
  }
  data=removeEmptyStringElements(data);


  /*console.log('---------------------------------------------------------------------------');
  console.log(data)
  console.log('---------------------------------------------------------------------------');
  console.log('---------------------------------------------------------------------------');

  let response = {
    "statusCode": 200,
    "body": JSON.stringify({status:false}),
  };

  callback(null, response);

  return;*/

  


  // Update the data IF already have 
  if(data.user_id){

    const params = {
      TableName: process.env.DYNAMODB_USER_TABLE,
      Key: {
        id: data.user_id,
      },
      ExpressionAttributeNames: {
        '#name': 'name',
      },
      ExpressionAttributeValues: {
        ':name': data.name,
        ':email': data.email,
        ':main_phone': data.main_phone,
        ':tax_id': data.tax_id,
        ':updatedAt': timestamp,
      },
      UpdateExpression: 'SET #name = :name, email = :email,main_phone = :main_phone,tax_id = :tax_id, updatedAt = :updatedAt',
      ReturnValues: 'ALL_NEW',
    };


    const update_application_params = {
      TableName: process.env.DYNAMODB_APPLICATION_TABLE,
      Key: {
        user_id: data.user_id,
      },
      ExpressionAttributeNames: {
        '#name': 'name',
        '#status': 'status',
      },
      ExpressionAttributeValues: {
        ':name': data.name,
        ':email': data.email,
        ':main_phone': data.main_phone,
        ':tax_id': data.tax_id,
        ':updatedAt': timestamp,
        ':status': data.status,
        ':national_id': data.national_id,
        ':company_name': data.company_name,
        ':mobile_phone': data.mobile_phone,
        ':city': data.city,
        ':district': data.district,
        ':address': data.address,
        ':independent_electrical': data.independent_electrical,
        ':installation_location': data.installation_location,
        ':already_participated': data.already_participated,
        ':taipower_bills_images': data.taipower_bills_images,
        ':license_images': data.license_images,
        ':rental_agreement_images': data.rental_agreement_images,
        ':interior': data.interior,
        ':exterior': data.exterior,
        ':referral': data.referral,
        ':lights': data.lights,
        ':video': data.video,
        ':taipower_primary_account': data.taipower_primary_account,
        ':taipower_secondary_accounts': data.taipower_secondary_accounts,
        ':auth_completed_images': data.auth_completed_images,
        ':electrical_panel_quantity': data.electrical_panel_quantity,
        ':electrical_panel_distance': data.electrical_panel_distance,
        ':referral_partner': data.referral_partner,
        ':lighting_color': data.lighting_color,
        ':ip': ip        
      },
      UpdateExpression: 'SET #name = :name, email = :email,main_phone = :main_phone,tax_id = :tax_id,#status = :status,national_id = :national_id,company_name = :company_name,mobile_phone = :mobile_phone,city = :city,district = :district,address = :address,independent_electrical = :independent_electrical,installation_location = :installation_location,already_participated = :already_participated,taipower_bills_images = :taipower_bills_images,license_images = :license_images,rental_agreement_images = :rental_agreement_images,interior = :interior,exterior = :exterior,referral = :referral,lights = :lights,video = :video,ip = :ip,taipower_primary_account = :taipower_primary_account,taipower_secondary_accounts = :taipower_secondary_accounts,auth_completed_images = :auth_completed_images,electrical_panel_quantity = :electrical_panel_quantity,electrical_panel_distance = :electrical_panel_distance,referral_partner = :referral_partner,lighting_color = :lighting_color, updatedAt = :updatedAt',
      ReturnValues: 'ALL_NEW',
    };

    // update the users in the database
    dynamodb.update(params, (error, result) => {
      if (error) {
        console.error(error);
        callback(null, {
          statusCode: error.statusCode || 501,
          headers: headers,
          body: JSON.stringify({ status: false,'message':error })
        });
        return;
      }
    });

    dynamodb.update(update_application_params, (error, result) => {
      if (error) {
        console.error(error);
        callback(null, {
          statusCode: error.statusCode || 501,
          headers: headers,
          body: JSON.stringify({ status: false,'message':error})
        });
        return;
      }

      // create a response
      const response = {
        statusCode: 200,
        headers: headers,
        body: JSON.stringify({ status: true ,data:result.Attributes}),
      };
      callback(null, response);
    });

  }else{

     const params = {
      TableName: process.env.DYNAMODB_USER_TABLE,
      Item: {
        id: uuid.v1(),
        name: data.name,
        email: data.email,
        nickname: " ",
        main_phone: data.main_phone,
        tax_id: data.tax_id,
        role: "customer",
        createdAt: timestamp,
        updatedAt: timestamp,
      },
    };

    const application_params = {
      TableName: process.env.DYNAMODB_APPLICATION_TABLE,
      Item: {
        // id: uuid.v1(),
        user_id: params.Item.id,
        status: data.status,
        name: data.name,
        email: data.email,
        main_phone: data.main_phone,
        tax_id: data.tax_id,
        national_id: data.national_id,
        company_name: data.company_name,
        mobile_phone: data.mobile_phone,
        city: data.city,
        district: data.district,
        address: data.address,
        independent_electrical: data.independent_electrical,
        installation_location: data.installation_location,
        already_participated: data.already_participated,
        taipower_bills_images: data.taipower_bills_images,
        taipower_primary_account: data.taipower_primary_account,
        taipower_secondary_accounts: data.taipower_secondary_accounts,
        license_images: data.license_images,
        rental_agreement_images: data.rental_agreement_images,
        auth_completed_images: data.auth_completed_images,
        interior: data.interior,
        exterior: data.exterior,
        electrical_panel_quantity: data.electrical_panel_quantity,
        electrical_panel_distance: data.electrical_panel_distance,
        referral: data.referral,
        referral_partner: data.referral_partner,
        lights: data.lights,
        video: data.video,
        lighting_color: data.lighting_color,
        ip: ip,
        createdAt: timestamp,
        updatedAt: timestamp,
      },
    };

    dynamodb.put(params, (error) => {
      // handle potential errors
      if (error) {
        console.error(error);
        callback(null, {
          statusCode: error.statusCode || 501,
          headers: headers,
          body: JSON.stringify({ status: false,'message':error })
        });
        return;
      }
    });

    dynamodb.put(application_params, (error) => {
     
      // handle potential errors
      if (error) {
        console.error(error);
        callback(null, {
          statusCode: error.statusCode || 501,
          headers: headers,
          body: JSON.stringify({ status: false ,'message':error})
        });
        return;
      }

      // create a response
      const response = {
        statusCode: 200,
        headers: headers,
        body:JSON.stringify({ status: true ,data:application_params.Item }),
      };
      callback(null, response);
    });
  }
};
