'use strict';
const uuid = require('uuid');
const dynamodb = require('../dynamodb');


module.exports.logout = (event, context, callback) => {

  // Set response headers to enable CORS (Cross-Origin Resource Sharing)
  const headers = {
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Allow-Credentials": true
  };
  
  const response = {
    statusCode: 200,
    headers: headers,
    body: JSON.stringify({ status: 'OK' })       
  };
  callback(null, response);

};

